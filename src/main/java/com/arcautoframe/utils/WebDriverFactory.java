package com.arcautoframe.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;



public class WebDriverFactory {
	
	
	static WebDriver driver;
	public static int maxPageLoadWait = 180;
	static String browser;
	/** 
	 * Method written for launching browser and landing the desired application.
	 * @param browser
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws AWTException 
	 */
	public static WebDriver browserLaunch(String browser,String url) throws IOException, AWTException
	{
		switch(browser)
		{
		case "chrome": 
			//System.setProperty("webdriver.chrome.driver", "GridConfig/chromedriver.exe");
			//URL driverSrc = WebDriverFactory.class.getResource("/drivers/win/chromedriver.exe");
			File dest = new File("./drivers/win/chromedriver.exe");
			//org.apache.commons.io.FileUtils.copyURLToFile(driverSrc, dest);
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			//driver = new ChromeDriver();
			//driver.manage().window().setSize(new Dimension(1440, 900));
			//((IJavaScriptExecutor)driver).ExecuteScript("window.resizeTo(1024, 768);");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(url);
			//driver.manage().window().maximize();
		case "firefox":
			
		}
		
		 /*if(browser.equalsIgnoreCase("firefox")) {
		 driver = new FirefoxDriver();
		 driver.get(url);
		 }
		 else if (browser.equalsIgnoreCase("chrome"))
		 {
			 File dest = new File("./drivers/win/chromedriver.exe"); 
			 System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			 ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				driver = new ChromeDriver( options );
				driver.get(url);
		 }
		 else if (browser.equalsIgnoreCase("safari"))
		 {
			 
		 }*/
		 
		
		
		return driver;
		
	}
	
	

}
